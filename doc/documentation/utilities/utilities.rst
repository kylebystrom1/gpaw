.. module:: gpaw.utilities


Utilities
=========

.. autoclass:: gpaw.utilities.partition.AtomPartition
    :members:

.. module:: gpaw.utilities.dipole
.. autofunction:: gpaw.utilities.dipole.dipole_matrix_elements
.. autofunction:: gpaw.utilities.dipole.dipole_matrix_elements_from_calc

.. module:: gpaw.utilities.ekin
.. autofunction:: gpaw.utilities.ekin.ekin


Electron localisation function (ELF)
====================================

.. module:: gpaw.elf
.. autofunction:: elf
.. autofunction:: elf_from_dft_calculation
